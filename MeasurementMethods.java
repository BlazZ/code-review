package brewchef.enolyse.com.brewchef.Model;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by blazzajec on 10/06/16.
 */
public class MeasurementMethods {

    private Networking mNetworking;
    private long count = 0;
    private long number = 0;

    final List<ArrayList<String>> arrayMeasurementData2 = new ArrayList<ArrayList<String>>();

    public interface MeasurementReadListener {
        void onDataRead(MeasurementObject measurementObject);
        void onDataCanceled();
    }
    public interface MeasurementSendListener {
        void onDataSend();
    }
    public interface ReadListener {
        void onDataRead(DataSnapshot dataSnapshot);
    }
    public interface ReadTankListener {
        void onDataRead(DataSnapshot dataSnapshot);
    }
    public interface ReadBreweryListener {
        void onDataRead(DataSnapshot dataSnapshot);
    }

    public void sendMeasurements(String comment, String beerId, String tankId, String typeId, String value, final MeasurementSendListener measurementSendListener ){
        long date = Time.getCurrentTimestamp();

        Map<String, Object> valueMap = new HashMap<String, Object>();
        ArrayList<String> arrayChilds = new ArrayList<>();

        valueMap.put("comment", comment);
        valueMap.put("beer_id", beerId);
        valueMap.put("tank_id", tankId);
        valueMap.put("type_id", typeId);
        valueMap.put("value", value);
        valueMap.put("date", date);

        arrayChilds.add(0, "measurements");
        arrayChilds.add(1, "all");
        arrayChilds.add(2, "push");

        mNetworking = new Networking();
        mNetworking.sendData(arrayChilds, valueMap, new Networking.SendDataListener() {
            @Override
            public void onDataSend(String uniqueId) {
                measurementSendListener.onDataSend();
            }
        });
    }

    // main function who call all functions bellow
    // get measurements of current user
    public void readMeasurements(int numOfmeasurements, String userId, MeasurementReadListener measurementListener){
        readBreweryId(numOfmeasurements, userId, measurementListener);
    }

    // read brewery id of current user
    private void readBreweryId(final int numOfMeasurements, String userId, final MeasurementReadListener measurementListener) {
        readBrewery(userId, new ReadBreweryListener() {
            @Override
            public void onDataRead(DataSnapshot dataSnapshot) {
                String breweryId = dataSnapshot.getValue().toString();
                readTankId(numOfMeasurements, breweryId, measurementListener);
            }
        });
    }

    // read tank id and name of current brewery
    private void readTankId(final int numOfMeasurements, final String breweryId, final MeasurementReadListener measurementListener) {
        final ArrayList<String> tankId = new ArrayList<>();
        final ArrayList<String> tankName = new ArrayList<>();

        readTanks(new ReadTankListener() {
            @Override
            public void onDataRead(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    if (child.child("brewery_id").getValue().toString().equals(breweryId)) {
                        tankName.add(child.child("name").getValue().toString());
                        tankId.add(child.getKey());
                        count++;
                    }
                }
                readMeasurementsAllTanks(numOfMeasurements, tankId.size(), tankId, tankName, measurementListener);
            }
        });
    }

    // get all measurements of all tanks from current brewery
    private void readMeasurementsAllTanks(int numOfMeasurements, int size, ArrayList<String> arrayTankIds, ArrayList<String> tankName, MeasurementReadListener measurementListener) {
        for(int i = 0; i < size; i++) {
            readMeasurementsOneTank(numOfMeasurements, arrayTankIds.get(i), tankName.get(i), measurementListener);
        }
    }

    // get all measurements from one tank
    private void readMeasurementsOneTank(final int numOfMeasurements, final String tankId, final String tankName, final MeasurementReadListener measurementListener) {
        final List<ArrayList<String>> arrayMeasurementData = new ArrayList<ArrayList<String>>();
        //final ArrayList<String[]> arrayMeasurementData= new ArrayList<>();
        final ArrayList<String> arrayChilds = new ArrayList<>();

        arrayChilds.add(0, "measurements");
        arrayChilds.add(1, "all");

        mNetworking = new Networking();
        mNetworking.readData(arrayChilds, new Networking.ReadDataListener() {
            @Override
            public void onDataRead(DataSnapshot dataSnapshot) {
                number = 0;
                for (Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator(); iterator.hasNext(); ) {
                    DataSnapshot child = iterator.next();
                    if (child.child("tank_id").getValue().toString().equals(tankId)) {
                        number++;
                        final String value = child.child("value").getValue().toString();
                        final String date = child.child("date").getValue().toString();
                        final String comment = child.child("comment").getValue().toString();
                        String typeId = child.child("type_id").getValue().toString();

                        arrayMeasurementData.add(new ArrayList<String>(Arrays.asList(tankId, value, tankName, comment, typeId, date)));
                        /*System.out.println("stevc: " + number + ", " + numOfMeasurements);
                        if (number != numOfMeasurements) {
                            arrayMeasurementData.add(new ArrayList<String>(Arrays.asList(tankId, value, tankName, comment, typeId, date)));
                            System.out.println("pogoj!");
                        }*/
                    }
                }
                readMeasurementTypes(arrayMeasurementData, measurementListener);
            }

            @Override
            public void onCancelled() {
                measurementListener.onDataCanceled();
            }
        });
    }

    private void readMeasurementTypes(final List<ArrayList<String>> arrayMeasurementData, final MeasurementReadListener measurementListener) {
        count = 0;
        for(int i = 0; i < arrayMeasurementData.size(); i++) {
            final String typeId = arrayMeasurementData.get(i).get(4);
            final String tankId = arrayMeasurementData.get(i).get(0);
            final String value = arrayMeasurementData.get(i).get(1);
            final String comment = arrayMeasurementData.get(i).get(3);
            final String tankName = arrayMeasurementData.get(i).get(2);
            final String date = arrayMeasurementData.get(i).get(5);

            readMeasurementTypes(new ReadListener() {
                @Override
                public void onDataRead(DataSnapshot dataSnapshot) {

                    String measurementType = dataSnapshot.child(typeId).child("name").getValue().toString();
                    String measurementUnit = dataSnapshot.child(typeId).child("unit").getValue().toString();

                    arrayMeasurementData2.add(new ArrayList<String>(Arrays.asList(tankId, value, comment, tankName, measurementType, measurementUnit, date)));
                    count ++;

                    if (count == arrayMeasurementData.size()){
                        MeasurementObject measurementObjectt = new MeasurementObject();
                        measurementObjectt.setMeasurements(arrayMeasurementData2);
                        count = 0;
                        measurementListener.onDataRead(measurementObjectt);
                    }
                }
            });
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////

    public void readMeasurementTypes(final ReadListener readListener){
        final ArrayList<String> arrayChilds = new ArrayList<>();

        arrayChilds.add(0, "measurements");
        arrayChilds.add(1, "type");


        mNetworking = new Networking();
        mNetworking.readData(arrayChilds, new Networking.ReadDataListener() {
            @Override
            public void onDataRead(DataSnapshot dataSnapshot) {
                readListener.onDataRead(dataSnapshot);
            }

            @Override
            public void onCancelled() {

            }
        });
    }

    public void readTanks(final ReadTankListener readTankListener){
        final ArrayList<String> arrayChilds = new ArrayList<>();

        arrayChilds.add(0, "tanks");

        mNetworking = new Networking();
        mNetworking.readData(arrayChilds, new Networking.ReadDataListener() {
            @Override
            public void onDataRead(DataSnapshot dataSnapshot) {
                readTankListener.onDataRead(dataSnapshot);
            }

            @Override
            public void onCancelled() {

            }
        });
    }

    public void  readBrewery(String userId, final ReadBreweryListener readBreweryListener){
        ArrayList<String> arrayChilds = new ArrayList<>();

        arrayChilds.add(0, "users");
        arrayChilds.add(1, "all");
        arrayChilds.add(2, userId);
        arrayChilds.add(3, "brewery_id");

        mNetworking = new Networking();
        mNetworking.readData(arrayChilds, new Networking.ReadDataListener() {
            @Override
            public void onDataRead(DataSnapshot dataSnapshot) {
                readBreweryListener.onDataRead(dataSnapshot);
            }

            @Override
            public void onCancelled() {

            }
        });
    }
}
