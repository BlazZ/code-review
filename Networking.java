package brewchef.enolyse.com.brewchef.Model;

import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by blazzajec on 02/06/16.
 */
public class Networking {

    private DatabaseReference mFirebaseDatabaseReference;
    private String uniqueId;

    public interface ReadDataListener {
        void onDataRead(DataSnapshot dataSnapshot);
        void onCancelled();
    }

    public interface SendDataListener {
        void onDataSend(String uniqueId);
    }

    public void sendData(ArrayList<String> arrayChilds, Map<String, Object> value, final SendDataListener sendUserDataListener){
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        for (int i=0; i < arrayChilds.size(); i++) {
             if (arrayChilds.get(i) == "push"){
                 mFirebaseDatabaseReference = mFirebaseDatabaseReference.push();
                 uniqueId = mFirebaseDatabaseReference.getKey();
             } else {
                 mFirebaseDatabaseReference = mFirebaseDatabaseReference.child(arrayChilds.get(i));
             }
        }

        mFirebaseDatabaseReference.setValue(value, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                //Log.d("onComplete", "dataSend");
                sendUserDataListener.onDataSend(uniqueId);
            }
        });
    }

    public void readData(ArrayList<String> arrayChilds, final ReadDataListener readUserDataListener){
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        for (int i=0; i < arrayChilds.size(); i++) {
            mFirebaseDatabaseReference = mFirebaseDatabaseReference.child(arrayChilds.get(i));
        }

        mFirebaseDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                readUserDataListener.onDataRead(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                readUserDataListener.onCancelled();
            }
        });
    }

    public void updateData(ArrayList<String> arrayChilds, Map<String, Object> value, final SendDataListener sendUserDataListener){
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        for (int i=0; i < arrayChilds.size(); i++) {
            mFirebaseDatabaseReference = mFirebaseDatabaseReference.child(arrayChilds.get(i));
        }

        mFirebaseDatabaseReference.updateChildren(value, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                //Log.d("onComplete", "UPDATED..." + databaseError);
                sendUserDataListener.onDataSend(uniqueId);
            }
        });
    }

    private void singleValueEventListener(ArrayList<String> arrayChilds, final ReadDataListener readUserDataListener){
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        mFirebaseDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Log.d("onDataChanged", ".......");
                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    String name = child.getValue().toString();
                    System.out.println(name);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}
