package brewchef.enolyse.com.brewchef.UI.fragment;

/**
 * Created by blazzajec on 22/06/16.
 */

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import brewchef.enolyse.com.brewchef.Model.BeerMethods;
import brewchef.enolyse.com.brewchef.Model.BeerObject;
import brewchef.enolyse.com.brewchef.Model.GenerateMethods;
import brewchef.enolyse.com.brewchef.Model.Networking;
import brewchef.enolyse.com.brewchef.R;
import brewchef.enolyse.com.brewchef.UI.AddBeerDialog;
import brewchef.enolyse.com.brewchef.UI.EmailSignIn;
import brewchef.enolyse.com.brewchef.UI.adapter.BeerAdapter;
import brewchef.enolyse.com.brewchef.UI.adapter.BeerAdapterOLD;

public class ProductionFragment extends Fragment {

    private View rootView;
    private TextView empty_recycler;

    List<ArrayList<String>> arrayBeerData = new ArrayList<ArrayList<String>>();

    private FloatingActionButton floatingActionButton;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private ProgressDialog progress;

    public ProductionFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.production, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*progress = new ProgressDialog(getActivity());
        progress.setMessage("Loading...");
        progress.show();*/

        empty_recycler = (TextView) rootView.findViewById(R.id.empty_recycler);

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        floatingActionButton = (FloatingActionButton) rootView.findViewById(R.id.floatingButton);

        //closeProgressDialog();



        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            Intent intent = new Intent(getActivity(), EmailSignIn.class);
            startActivity(intent);
            getActivity().finish();
            return;
        } else {
            // signed in
            String userId = mFirebaseUser.getUid();


            readAllBeers(userId);
            startCreateBeer(userId);
        }
    }
    // get all data of all beers
    private void readAllBeers(final String userId) {
        BeerMethods beer = new BeerMethods();
        beer.readBeers(userId, new BeerMethods.ReadBeerListener() {
            @Override
            public void onDataRead(BeerObject beerObject) {
                getBeersSortByDate(beerObject);
                if (arrayBeerData.size() == 0){
                    empty_recycler.setVisibility(View.VISIBLE);
                } else if (arrayBeerData.size() > 0){
                    empty_recycler.setVisibility(View.GONE);
                }
                generateList(userId);
                //progress.dismiss();
            }
        });
    }

    // This method generate list of all beers
    private void generateList(String userId){
        RecyclerView recList = (RecyclerView) rootView.findViewById(R.id.cardList);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        BeerAdapter ca = new BeerAdapter(createList(), arrayBeerData, userId);
        recList.setAdapter(ca);
    }

    // set name, URL and id for all residents
    private List<BeerDetails> createList() {
        final List<BeerDetails> result = new ArrayList<BeerDetails>();
        for (int i=0; i < arrayBeerData.size(); i++) {

            BeerDetails bd = new BeerDetails();
            bd.ciBeerName = arrayBeerData.get(i).get(0);
            bd.ciVolume = arrayBeerData.get(i).get(3);
            bd.ciCreated = arrayBeerData.get(i).get(4);
            bd.ciBreweryId = arrayBeerData.get(i).get(6);
            bd.ciTankName = arrayBeerData.get(i).get(7);
            bd.ciBeerState = arrayBeerData.get(i).get(8);
            bd.ciBeerId = arrayBeerData.get(i).get(9);
            bd.ciBeerLogo = setPicture(arrayBeerData.get(i).get(11));
            if (arrayBeerData.get(i).get(12) != null) {
                bd.ciLastMeasurementValue = arrayBeerData.get(i).get(12);
                bd.ciLastMeasurementDate = arrayBeerData.get(i).get(13);
            } else {
                bd.ciLastMeasurementValue = "/";
                bd.ciLastMeasurementDate = "/";
            }
            result.add(bd);
        }
        return result;
    }

    // create bitmap picture from string and display it
    private Bitmap setPicture(String imageFileString){
        byte[] decodedString = Base64.decode(imageFileString, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        //vBeerLogo.setImageBitmap(bitmap);
        return bitmap;
    }

    public class BeerDetails {
        public String ciBeerName;
        public String ciVolume;
        public String ciCreated;
        public String ciBreweryId;
        public String ciTankName;
        public String ciBeerState;
        public Bitmap ciBeerLogo;
        public String ciBeerId;
        public String ciLastMeasurementValue;
        public String ciLastMeasurementDate;
    }

    // get beers data and sort array by date (newest to oldest)
    private void getBeersSortByDate(BeerObject beerObject) {
        arrayBeerData.clear();
        for (int i = 0; i < beerObject.getBeers().size(); i++) {
            //for (int i = 0; i < numOfMeasurements; i++) {
            ArrayList<String> arrayList = beerObject.getBeers().get(i);

            String name = arrayList.get(0);
            String recipe = arrayList.get(1);
            String batch = arrayList.get(2);
            String volume = arrayList.get(3);
            String created = arrayList.get(4);
            String updated = arrayList.get(5);
            String brewery_id = arrayList.get(6);
            String tank_name = arrayList.get(7);
            String beer_state = arrayList.get(8);
            String beer_id = arrayList.get(9);
            String description = arrayList.get(10);
            String beer_logo = arrayList.get(11);
            String lastMeasurementValue = arrayList.get(12);
            String lastMeasurementDate = arrayList.get(13);

            arrayBeerData.add(new ArrayList<String>(Arrays.asList(name, recipe, batch, volume, created, updated, brewery_id,
                    tank_name, beer_state, beer_id, description, beer_logo, lastMeasurementValue, lastMeasurementDate)));
        }

        Collections.sort(arrayBeerData, new Comparator<ArrayList<String>>() {
            @Override
            public int compare(ArrayList<String> o1, ArrayList<String> o2) {
                return o2.get(4).compareTo(o1.get(4));
            }
        });
    }
    // button - on click listener - start add beer activity
    private void startCreateBeer(final String userId){
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddBeerDialog.class);
                intent.putExtra("userId", userId);
                startActivity(intent);
            }
        });
    }

    private void signOut() {
        mFirebaseAuth.signOut();
        Intent intent = new Intent(getActivity(), EmailSignIn.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.sign_out) {
            signOut();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
